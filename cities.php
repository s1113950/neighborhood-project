<!DOCTYPE html>
<html>

<head>
    <title>Ranked Cities</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="general.css" media="screen" /> <!--this can be changed later -->
</head>

<body>

<h2>Choose a city</h2>

<?php

include "config.php";

$tbl = "Cities";

$query = $link->prepare("SELECT * FROM $tbl ORDER BY name ASC") or die("Error preparing query");
$query->execute() or die("Error executing query");

$query->bind_result($name) or die("Error binding result");

while ($query->fetch()) {
    echo "<a href='http://FAKELINK.COM/RANKS?city=" . $name . "'>" . $name . "</a><br>";
}

?>

</body>

</html>